import pytest

from tasks import add


@pytest.mark.task
@pytest.mark.parametrize(
    "a,b,c",
    [
        (1, 2, 3),
        (2, 2, 4),
        (4, 5, 9),
        (10, 8, 18),
        (7, 6, 13),
    ],
)
def test_add(a, b, c):
    assert add(a, b) == c
