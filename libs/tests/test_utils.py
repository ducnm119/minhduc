from datetime import datetime, timedelta

import pytest

from libs.utils import easy_divide, easy_sum


@pytest.mark.fun
@pytest.mark.parametrize(
    "a,b,c",
    [
        (1, 2, 3),
        (2, 2, 4),
        (4, 5, 9),
        (10, 8, 18),
        (7, 6, 13),
    ],
)
def test_easy_sum_success(a, b, c, session_fixture, module_fixture):
    assert easy_sum(a, b) == c
    assert session_fixture == "hello"
    assert module_fixture > (datetime.now() - timedelta(days=1)).date()


@pytest.mark.fun
def test_easy_divide_raises():
    with pytest.raises(ZeroDivisionError):
        easy_divide(10, 0)


@pytest.mark.fun
@pytest.mark.parametrize(
    "a,b",
    [
        (1988, 34),
        (1989, 33),
    ],
)
def test_easy_sum_success_with_fixture_1(
    a, b, function_fixture, session_fixture, module_fixture
):
    assert easy_sum(a, b) == function_fixture
    assert session_fixture == "hello"
    assert module_fixture > (datetime.now() - timedelta(days=1)).date()
