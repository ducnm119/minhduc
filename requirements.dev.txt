pre-commit==2.20.0

# debug
ipython==7.24.1
django-debug-toolbar==3.2.1

# test
coverage==6.4.2
factory-boy==3.2.0
pytest-django==4.5.2
pytest-cov==3.0.0
