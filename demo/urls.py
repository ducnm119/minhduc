"""demo URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path

urlpatterns = [
    re_path(r"^admin/", admin.site.urls),
    re_path(r"^v1/", include(("v1.urls", "v1"))),
]
urlpatterns = (
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + urlpatterns
)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
