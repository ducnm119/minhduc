import django_filters

from warehouse.models import Category


class CategoryFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(
        field_name="name", lookup_expr="icontains"
    )

    class Meta:
        model = Category
        fields = ["name"]
