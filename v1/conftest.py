import pytest

from warehouse.factories import CategoryFactory, ProductFactory


@pytest.fixture
def default_category():
    return CategoryFactory.create()


@pytest.fixture
def default_product(default_category):
    return ProductFactory.create(category=default_category)
