from rest_framework import viewsets

from warehouse.models import Category

from ..filters import CategoryFilter
from ..serializers import CategorySerializer


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    filterset_class = CategoryFilter
    queryset = Category.objects.all()
