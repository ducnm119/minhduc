import pytest
from django.urls import reverse

from warehouse.factories import CategoryFactory

pytestmark = pytest.mark.django_db


@pytest.mark.api
def test_categories_list(client, json_headers, default_category):
    res = client.get(
        reverse("v1:categories-list"),
        **json_headers,
    )

    assert res.status_code == 200

    ids = [product["id"] for product in res.json()["results"]]
    assert default_category.id in ids


@pytest.mark.api
def test_category_detail(client, json_headers, default_category):
    res = client.get(
        reverse("v1:categories-detail", args=[default_category.id]),
        **json_headers,
    )

    assert res.status_code == 200

    data = res.json()
    assert data["name"] == default_category.name
    assert data["id"] == default_category.id


@pytest.mark.api
def test_category_patch(client, json_headers, default_category):
    res = client.patch(
        reverse("v1:categories-detail", args=[default_category.id]),
        {"name": "new name"},
        **json_headers,
    )

    assert res.status_code == 200

    data = res.json()
    default_category.refresh_from_db()
    assert data["name"] == default_category.name


@pytest.mark.api
def test_category_put(client, json_headers, default_category):
    res = client.put(
        reverse("v1:categories-detail", args=[default_category.id]),
        {
            "name": "new name",
            "address": "address 123",
        },
        **json_headers,
    )

    assert res.status_code == 200

    data = res.json()
    default_category.refresh_from_db()
    assert data["name"] == default_category.name
    assert data["address"] == default_category.address


@pytest.mark.api
def test_category_delte(client, json_headers):
    another_category = CategoryFactory.create()
    res = client.delete(
        reverse("v1:categories-detail", args=[another_category.id]),
        **json_headers,
    )

    assert res.status_code == 204
