# Demo Django-Pytest project

Author: Joe - Vu Quang Hoa

Email: hoa.vuquang@glinteco.com

Website: https://glinteco.com

This project is a sample Django project with pytest


## 1. Local installation

### 1.1 Pre-requisite packages

```
pyenv
virtualenv
```

### 1.2 Install packages

```sh
pyenv virtualenv 3.10 demo
pyenv activate demo
```

Then install application packages

```sh
pip install -r requirements.txt
pip install -r requirements.dev.txt  # for local only
```

in the case you meet error when install psycopg2 error, you must install psycopg2-binary, or check info in link  <https://www.psycopg.org/docs/install.html>).

### 1.3 Create local database name and user

Download the psql to your machine and start it, then create database + user

```sql
DROP DATABASE IF EXISTS demo;

CREATE DATABASE demo;

CREATE ROLE demo WITH LOGIN PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE demo TO demo;
ALTER USER demo SUPERUSER;
```


### 1.4 Create `demo/settings/.env`

```sh
cp demo/settings/.env.example demo/settings/.env
```

alter parameters to match your environment

### 1.5 Migrate database

```
python manage.py migrate
```

### 1.6 Run application

```
python manage.py runserver
```

### 1.7 Run Celery background tasks (including celery-beat)

```
celery -A demo beat -l info
```

## 2. Pre-commit

In the root directory

```sh
pre-commit install
```


## 3. Run tests

```sh
pytest
pytest -m fun  # test fun tests
pytest -m api  # test api tests
pytest -m task  # test task tests
```
