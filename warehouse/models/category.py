from django.db import models

from libs.base_model import BaseModel


class Category(BaseModel):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
